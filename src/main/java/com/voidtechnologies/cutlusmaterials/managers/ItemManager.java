package com.voidtechnologies.cutlusmaterials.managers;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.managers.CivilizationManager;
import com.voidtechnologies.cutlusmaterials.CultusMaterials;
import com.voidtechnologies.cutlusmaterials.items.ShapedRecipe;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Dylan Holmes
 * @date Feb 1, 2018
 */
public class ItemManager {

    public static ArrayList<ShapedRecipe> recipes = new ArrayList<>();

    public static void setCultusItem(ItemStack item, String civ, String name, boolean sb, int craftingTier, boolean hideEnchants) {
        ItemMeta im = item.getItemMeta();
        List<String> lore = im.getLore();

        if (name != null && !name.equals("")) {
            im.setDisplayName(name);
        }

        if (lore == null) {
            lore = new LinkedList<>();
        }

        lore.add(CultusMaterials.ITEM_IDENTIFIER);
        if (sb) {
            lore.add(CultusMaterials.SOUL_BOUND_IDENTIFIER);
        }
        if (civ != null && !civ.equals("")) {
            Civilization cCiv = CivilizationManager.getCivByName(civ);
            lore.add(CultusMaterials.CIV_IDENTIFIER + cCiv.getCivName().toUpperCase());
        }
        if (craftingTier != 0) {
            lore.add(CultusMaterials.CRAFT_TIER_IDENTIFIER + " " + craftingTier);
            im.addEnchant(Enchantment.DURABILITY, 0, true);
        }
        if (hideEnchants) {
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        im.setLore(lore);
        item.setItemMeta(im);
    }

    public static void addRecipe(ShapedRecipe recipe) {
        recipes.add(recipe);
    }

    public static void removeRecipe(ShapedRecipe recipe) {
        recipes.remove(recipe);
    }
}
