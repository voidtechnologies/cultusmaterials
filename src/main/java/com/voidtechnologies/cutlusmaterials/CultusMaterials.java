package com.voidtechnologies.cutlusmaterials;

import com.voidtechnologies.cutlusmaterials.listeners.CMListener;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Dylan Holmes
 * @date Jan 31, 2018
 */
public class CultusMaterials extends JavaPlugin {

    public static final String ITEM_IDENTIFIER = "§I§T§E§M" + ChatColor.RESET;
    public static final String SOUL_BOUND_IDENTIFIER = (ChatColor.RESET.toString() + ChatColor.LIGHT_PURPLE.toString() + "Soul bound");
    public static final String CIV_IDENTIFIER = (ChatColor.RESET.toString() + ChatColor.WHITE);
    public static final String CRAFT_TIER_IDENTIFIER = (ChatColor.RESET.toString() + ChatColor.GRAY) + "Crafting Matieral Tier";


    @Override
    public void onEnable() {
        CMListener cmListener = new CMListener();
        this.getServer().getPluginManager().registerEvents(cmListener, this);
    }

    @Override
    public void onDisable() {
    }
}
