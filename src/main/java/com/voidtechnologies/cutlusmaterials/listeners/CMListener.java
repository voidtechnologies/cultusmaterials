package com.voidtechnologies.cutlusmaterials.listeners;

import com.voidtechnologies.cutlusmaterials.items.CultusItem;
import com.voidtechnologies.cutlusmaterials.items.ShapedRecipe;
import com.voidtechnologies.cutlusmaterials.managers.ItemManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Feb 1, 2018
 */
public class CMListener implements Listener {

    private HashMap<UUID, List<ItemStack>> soulBoundItems = new HashMap<>();

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDeath(PlayerDeathEvent event) {
        List<ItemStack> drops = event.getDrops();
        List<ItemStack> ableToDrop = new ArrayList<>();
        List<ItemStack> keepSakes = new ArrayList<>();
        for (ItemStack item : drops) {
            CultusItem cultusItem = CultusItem.getCultusItem(item);
            if (cultusItem == null) {
                ableToDrop.add(item);
                continue;
            }

            if (cultusItem.isSoulBound()) {
                keepSakes.add(item);
                continue;
            }
            ableToDrop.add(item);
        }
        drops.clear();
        for (ItemStack is : ableToDrop) {
            drops.add(is);
        }
        soulBoundItems.put(event.getEntity().getUniqueId(), keepSakes);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlaceCraftingMaterial(BlockPlaceEvent event) {
        CultusItem ci = CultusItem.getCultusItem(event.getItemInHand());
        if (ci == null) {
            return;
        }
        if (ci.getCraftingTier() > 0) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerRespawn(PlayerRespawnEvent event) {

        List<ItemStack> keepSakes = soulBoundItems.get(event.getPlayer().getUniqueId());
        if (keepSakes == null) {
            return;
        }
        for (ItemStack is : keepSakes) {
            event.getPlayer().getInventory().addItem(is);
        }
        soulBoundItems.put(event.getPlayer().getUniqueId(), null);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onItemCraft(InventoryClickEvent event) {
        Inventory inv = event.getClickedInventory();
        if (inv == null || !inv.getType().equals(InventoryType.WORKBENCH)) {
            return;
        }
        if (!event.getSlotType().equals(SlotType.RESULT)) {
            return;
        }
        if (event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
        }
        ItemStack result = event.getCurrentItem();
        for (ShapedRecipe recipes : ItemManager.recipes) {
            if (recipes.resultMatches(result)) {
                event.setCancelled(true);
                recipes.craftItem((Player) event.getWhoClicked(), (CraftingInventory) inv, event.getAction());
                return;
            }
        }
    }

    @EventHandler
    public void onPrepareCraft(PrepareItemCraftEvent event) {
        CraftingInventory inv = event.getInventory();

        if (!inv.getType().equals(InventoryType.WORKBENCH)) {
            return;
        }

        ItemStack[] matrix = inv.getMatrix();
        HashMap<Integer, ItemStack> matrixMap = new HashMap<>();
        boolean cultusItems = false;
        for (int i = 0; i < matrix.length; i++) {
            ItemStack is = matrix[i];
            if (is == null) {
                continue;
            }
            if (!cultusItems) {
                cultusItems = CultusItem.getCultusItem(is) != null;
            }
            matrixMap.put(i, is);
        }

        for (ShapedRecipe recipes : ItemManager.recipes) {
            if (recipes.matches(matrixMap)) {
                inv.setResult(recipes.getResult());
                return;
            }
        }
        if (cultusItems) {
            inv.setResult(new ItemStack(Material.AIR));
        }
    }
}
