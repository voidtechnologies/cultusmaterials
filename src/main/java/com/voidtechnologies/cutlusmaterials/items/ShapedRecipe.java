package com.voidtechnologies.cutlusmaterials.items;

import com.voidtechnologies.cutlusmaterials.managers.ItemManager;
import java.util.HashMap;
import java.util.Map.Entry;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Dylan Holmes
 * @date Feb 2, 2018
 */
public class ShapedRecipe extends Recipe {

    private String[] shape;
    private HashMap<Character, ItemStack> ingriedentsMap = new HashMap<>();
    private HashMap<Integer, Character> characterIndex = new HashMap<>();

    private boolean valid = false;

    public ShapedRecipe(String[] shape, ItemStack result) {
        super(result);
        this.shape = shape;
    }

    public String[] getShape() {
        return shape;
    }

    private void processShape(String[] rows) {

        int i = 0;
        for (String row : rows) {
            if (row.length() != 3) {
                valid = false;
                return;
            }

            for (Character c : row.toCharArray()) {
                characterIndex.put(i++, c);
            }
        }
    }

    public void setIngredient(Character c, ItemStack ing) {
        ingriedentsMap.put(c, ing);
    }

    public void setIngredient(Character c, Material mat) {
        ingriedentsMap.put(c, new ItemStack(mat));
    }

    public ItemStack getItemStackAt(int index) {
        ItemStack is = ingriedentsMap.get(characterIndex.get(index));
        if (is == null) {
            return new ItemStack(Material.AIR);
        }
        return is;
    }

    public ItemStack getItemStackByChar(Character c) {
        ItemStack is = ingriedentsMap.get(c);
        if (is == null) {
            return new ItemStack(Material.AIR);
        }
        return is;
    }

    public boolean matches(HashMap<Integer, ItemStack> matrix) {
        for (Entry<Integer, Character> entry : characterIndex.entrySet()) {
            ItemStack recipeItem = getItemStackByChar(entry.getValue());
            if (recipeItem == null) {
                return false;
            }
            ItemStack compare = matrix.get(entry.getKey());
            if (compare == null && recipeItem.getType().equals(Material.AIR)) {
                continue;
            } else if (compare == null) {
                return false;
            }

            if (!compare.getType().equals(recipeItem.getType())) {
                return false;
            }

            if ((recipeItem.hasItemMeta() && !compare.hasItemMeta()) || (compare.hasItemMeta() && !recipeItem.hasItemMeta())) {
                return false;
            }

            if (!compare.hasItemMeta() && !recipeItem.hasItemMeta()) {
                continue;
            }
            ItemMeta im = recipeItem.getItemMeta();
            ItemMeta imComp = compare.getItemMeta();

            if (!im.getDisplayName().equals(imComp.getDisplayName())) {
                return false;
            }
        }
        return true;
    }

    public boolean resultMatches(ItemStack stack) {
        if (stack == null || stack.getType().equals(Material.AIR)) {
            return false;
        }

        if (getResult().hasItemMeta() != stack.hasItemMeta()) {
            return false;
        }

        if (!getResult().getType().equals(stack.getType())) {
            return false;
        }
        return getResult().getItemMeta().getDisplayName().equals(stack.getItemMeta().getDisplayName());
    }

    public void register() {
        processShape(shape);
        ItemManager.addRecipe(this);
    }

    public void craftItem(Player player, CraftingInventory inv, InventoryAction action) {
        player.getInventory().addItem(getResult());

        for (ItemStack stack : inv.getContents()) {
            stack.setAmount(stack.getAmount() - 1);
        }
        ItemStack[] matrix = inv.getMatrix();
        HashMap<Integer, ItemStack> matrixMap = new HashMap<>();
        for (int i = 0; i < matrix.length; i++) {
            ItemStack is = matrix[i];
            if (is == null) {
                continue;
            }
            matrixMap.put(i, is);
        }
        if (this.matches(matrixMap)) {
            inv.setResult(getResult());
        } else {
            inv.setResult(new ItemStack(Material.AIR));
        }
    }
}