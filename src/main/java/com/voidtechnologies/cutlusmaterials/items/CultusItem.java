package com.voidtechnologies.cutlusmaterials.items;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.managers.CivilizationManager;
import com.voidtechnologies.cutlusmaterials.CultusMaterials;
import java.util.List;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Dylan Holmes
 * @date Feb 1, 2018
 */
public class CultusItem {

    private boolean soulBound;
    private String name;
    private ItemStack handle;
    private Civilization civ;
    private int craftingTier;

    private CultusItem(ItemStack handle, String name, boolean soulBound, Civilization civ, int craftingTier) {
        this.handle = handle;
        this.name = name;
        this.soulBound = soulBound;
        this.civ = civ;
        this.craftingTier = craftingTier;
    }

    public boolean isSoulBound() {
        return soulBound;
    }

    public String getName() {
        return name;
    }

    public ItemStack getHandle() {
        return handle;
    }

    public Civilization getCiv() {
        return civ;
    }

    public static CultusItem getCultusItem(ItemStack stack) {
        ItemMeta im = stack.getItemMeta();
        if (im == null) {
            return null;
        }
        List<String> lore = im.getLore();
        if (lore == null || !lore.contains(CultusMaterials.ITEM_IDENTIFIER)) {
            return null;
        }
        String name = im.getDisplayName();
        boolean soulBound = lore.contains(CultusMaterials.SOUL_BOUND_IDENTIFIER);
        Civilization civ = null;
        int craftingTier = 0;
        for (String line : lore) {
            if (line.startsWith(CultusMaterials.CIV_IDENTIFIER)) {
                String civName = line.replace(CultusMaterials.CIV_IDENTIFIER, "").replace(" ", "").toLowerCase();
                civ = CivilizationManager.getCivByName(civName);
            }
            if (line.startsWith(CultusMaterials.CRAFT_TIER_IDENTIFIER)) {
                craftingTier = Integer.valueOf(line.replace(CultusMaterials.CRAFT_TIER_IDENTIFIER, "").replace(" ", ""));
            }
        }
        return new CultusItem(stack, name, soulBound, civ, craftingTier);
    }

    public int getCraftingTier() {
        return craftingTier;
    }
}