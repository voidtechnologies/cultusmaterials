package com.voidtechnologies.cutlusmaterials.items;

import org.bukkit.inventory.ItemStack;

/**
 * Represents a shapeless (ie normal) crafting recipe.
 */
public class Recipe {

    private ItemStack result;

    public Recipe(ItemStack result) {
        this.result = result;
    }

    public ItemStack getResult() {
        return result;
    }
}